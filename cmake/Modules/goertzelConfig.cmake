INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_GOERTZEL goertzel)

FIND_PATH(
    GOERTZEL_INCLUDE_DIRS
    NAMES goertzel/api.h
    HINTS $ENV{GOERTZEL_DIR}/include
        ${PC_GOERTZEL_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GOERTZEL_LIBRARIES
    NAMES gnuradio-goertzel
    HINTS $ENV{GOERTZEL_DIR}/lib
        ${PC_GOERTZEL_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GOERTZEL DEFAULT_MSG GOERTZEL_LIBRARIES GOERTZEL_INCLUDE_DIRS)
MARK_AS_ADVANCED(GOERTZEL_LIBRARIES GOERTZEL_INCLUDE_DIRS)

