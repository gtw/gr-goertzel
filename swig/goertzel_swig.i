/* -*- c++ -*- */

#define GOERTZEL_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "goertzel_swig_doc.i"

%{
#include "goertzel/defsk.h"
#include "goertzel/cw.h"
%}


%include "goertzel/defsk.h"
%include "goertzel/cw.h"
GR_SWIG_BLOCK_MAGIC2(goertzel, defsk);
GR_SWIG_BLOCK_MAGIC2(goertzel, cw);
