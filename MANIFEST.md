title: Goertzel filter module
brief: Various blocks for implementing Goertzel filters
tags: # Tags are arbitrary, but look at CGRAN what other authors are using
  - sdr
author:
  - Gary Wong <gtw@flux.utah.edu>
copyright_owner:
  - Flux Research Group, University of Utah
license:
#repo: # Put the URL of the repository here, or leave blank for default
#website: <module_website> # If you have a separate project website, put it here
#icon: <icon_url> # Put a URL to a square image here that will be used as an icon on CGRAN
---
