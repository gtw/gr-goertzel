/* -*- c++ -*- */
/* 
 * Copyright 2017 University of Utah and the Flux Group.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "defsk_impl.h"

#define DECIMATE 0x40

#define GOERTZEL_DEBUG 1

#if GOERTZEL_DEBUG
#include <iostream>
#endif

namespace gr {
  namespace goertzel {
    filter::filter( double w0, double decay0 ) {
	w = w0;
	cos_w = cos( w );
	decay = decay0;
	s2 = 0.0;
	s1 = 0.0;
    }
      
    void filter::feed( gr_complexd s ) {
	s0 = s + 2.0 * cos_w * s1 - s2;

	s2 = s1 * decay;
	s1 = s0 * decay;
    }
      
    double filter::get_freq() {
	return w;
    }
      
    void filter::set_freq( double w_new ) {
	w = w_new;
	cos_w = cos( w );
    }
      
    void filter::set_decay( double decay_new ) {
	decay = decay_new;
    }
      
    double filter::power() {
	return std::abs( s2 * s2 + s1 * s1 - 2.0f * cos_w * s1 * s2 );
    }
      
    defsk::sptr defsk::make( double fc, double rate, double carrier,
			     double shift ) {
	return gnuradio::get_initial_sptr(
	    new defsk_impl( fc, rate, carrier, shift ) );
    }

    defsk_impl::defsk_impl( double fc_, double rate_, double carrier,
			    double shift_ )
        : gr::sync_decimator( "defsk",
			      gr::io_signature::make( 1, 1,
						      sizeof (gr_complex) ),
			      gr::io_signature::make2( 1, 2, sizeof (float),
						       sizeof (float) ), 64 ),
        fc( fc_ ),
        rate( rate_ ),
        shift( shift_ ),
        fl( ( carrier - shift * 0.5 - fc ) * 2.0f * M_PI / rate, 0.99996 ),
        fh( ( carrier + shift * 0.5 - fc ) * 2.0f * M_PI / rate, 0.99996 ),
        fll( ( carrier - shift * 1.5 - fc ) * 2.0f * M_PI / rate, 0.99996 ),
        fhh( ( carrier + shift * 1.5 - fc ) * 2.0f * M_PI / rate, 0.99996 ),
	flp( fl.get_freq() + 0.0005, 0.995 ),
	flm( fl.get_freq() - 0.0005, 0.995 ),
	fhp( fh.get_freq() + 0.0005, 0.995 ),
	fhm( fh.get_freq() - 0.0005, 0.995 ),
        diff( 0.0 ),
	index( 0 ),
	period( 250000 ),
	phase( 0 ),
        locked( 0 ),
	phase_err( 0.0 )
    {
	comp[ 0 ] = comp[ 1 ] = comp[ 2 ] = comp[ 3 ] = 0.0;
    }

    defsk_impl::~defsk_impl()
    {
    }

    int
    defsk_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      float *out = (float *) output_items[0];
      float *freq_out = (float *) output_items[1];
      int done;
      
      for( done = 0; done < noutput_items; done++ ) {
	  int i;
	  float newer, powerp, powerm;
	  
	  for( i = 0; i < DECIMATE; i++, in++ ) {
	      gr_complexd ind = gr_complexd( *in );
	      
	      fl.feed( ind );
	      fh.feed( ind );
	      flp.feed( ind );
	      flm.feed( ind );
	      fhp.feed( ind );
	      fhm.feed( ind );
	      fll.feed( ind );
	      fhh.feed( ind );
	  }

	  index += DECIMATE;

	  if( index > period ) {
	      index -= period;
	      phase++;
	      phase &= 3;

	      if( !phase ) {
		  if( phase_err < -0.01 )
		      period--;
		  else if( phase_err > 0.01 )
		      period++;
	      }
	  }

	  float cur = phase & 2 ? fh.power() - fl.power() :
	      fl.power() - fh.power();
	  
	  *out++ = cur;	  
	  comp[ phase ] = ( comp[ phase ] * 1023.0 + cur ) / 1024.0;

	  phase_err = phase_err * 0.9999 +
	      ( ( comp[ 0 ] + comp[ 2 ] > comp[ 1 ] + comp[ 3 ] ) ?
		-0.0001 : 0.0001 );
	  
	  powerp = phase & 2 ? fhp.power() : flp.power();
	  powerm = phase & 2 ? fhm.power() : flm.power();
		
	  if( powerp > powerm ) {
	      double adj = powerp / powerm - 0.95;
	      if( !( adj < 50 ) )
		  adj = 50;

	      newer = 0.00001 * adj;
	  } else if( powerm > powerp ) {
	      double adj = powerm / powerp - 0.95;
	      if( !( adj < 50 ) )
		  adj = 50;

	      newer = -0.00001 * adj;
	  } else
	      newer = 0;

	  diff = ( diff * 15.0 + newer ) / 16.0;

	  if( locked )
	      diff /= 32.0;

	  diff = 0; /* FIXME disable tuning while debugging PLL */
	  
	  double f_new = fl.get_freq() + diff;
	  fl.set_freq( f_new );
	  /* FIXME set wp/wm closer to w (and decay slower) when good
	     quality lock achieved */
	  flp.set_freq( f_new + 0.0005 );
	  flm.set_freq( f_new - 0.0005 );
	  fh.set_freq( f_new + 20000 * M_PI / rate );
	  fhp.set_freq( f_new + 20000 * M_PI / rate + 0.0005 );
	  fhm.set_freq( f_new + 20000 * M_PI / rate - 0.0005 );
		       
	  if( output_items.size() >= 2 )
	      *freq_out++ = f_new * M_1_PI * 0.5 * rate;

	  locked = ( f_new * M_1_PI * 0.5 * rate ) > -495200 &&
	      ( f_new * M_1_PI * 0.5 * rate ) < -494800; /* FIXME ugly hack! */

#if GOERTZEL_DEBUG
	  static int n;

	  if( !( n++ & 0xFFF ) ) {
	      std::cerr << "output " << noutput_items << " w " << fl.get_freq() << std::endl;
	      std::cerr << " 0: " << comp[ 0 ] << std::endl;
	      std::cerr << " 1: " << comp[ 1 ] << std::endl;
	      std::cerr << " 2: " << comp[ 2 ] << std::endl;
	      std::cerr << " 3: " << comp[ 3 ] << std::endl;
	      std::cerr << ( locked ? "locked" : "not locked" ) << std::endl;
	      std::cerr << "phase_err: " << phase_err << " period: " << period << std::endl;
	  }
#endif
      }

      // Tell runtime system how many output items we produced.
      return done;
    }

  } /* namespace goertzel */
} /* namespace gr */

