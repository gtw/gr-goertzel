/* -*- c++ -*- */
/* 
 * Copyright 2017 University of Utah and the Flux Group.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "cw_impl.h"

#define GOERTZEL_DEBUG 0

#if GOERTZEL_DEBUG
#include <iostream>
#endif

namespace gr {
  namespace goertzel {
    inf_filter::inf_filter( double w0 ) {
	w = w0;
	cos_w = cos( w );
	s2 = 0.0;
	s1 = 0.0;
    }
      
    void inf_filter::feed( gr_complexd s ) {
	s0 = s + 2.0 * cos_w * s1 - s2;
	s2 = s1;
	s1 = s0;
    }
      
    void inf_filter::reset() {
	s2 = 0.0;
	s1 = 0.0;	
    }
      
    double inf_filter::get_freq() {
	return w;
    }
      
    void inf_filter::set_freq( double w_new ) {
	w = w_new;
	cos_w = cos( w );
    }
      
    double inf_filter::power() {
	return std::abs( s2 * s2 + s1 * s1 - 2.0f * cos_w * s1 * s2 );
    }
      
    cw::sptr cw::make( double fc, double rate, int decimation,
		       double carrier ) {
	return gnuradio::get_initial_sptr(
	    new cw_impl( fc, rate, decimation, carrier ) );
    }

      cw_impl::cw_impl( double fc_, double rate_, int decimate_,
			double carrier )
        : gr::sync_decimator( "cw",
			      gr::io_signature::make( 1, 1,
						      sizeof (gr_complex) ),
			      gr::io_signature::make2( 1, 2, sizeof (float),
						       sizeof (float) ),
			      decimate_ ),
        fc( fc_ ),
        rate( rate_ ),
        decimate( decimate_ ),
        f( ( carrier - fc ) * 2.0f * M_PI / rate ),
        n( 0 )
    {
    }

    cw_impl::~cw_impl()
    {
    }

    int
    cw_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];
      float *out = (float *) output_items[0];
      float *freq_out = (float *) output_items[1];
      int done;
      
      for( done = 0; done < noutput_items; done++ ) {
	  int i;
	  
	  for( i = 0; i < decimate; i++, in++ ) {
	      gr_complexd ind = gr_complexd( *in );

	      f.feed( ind );
	  }

	  *out++ = log10( f.power() ) * 10.0;
	  f.reset();
	  
#if GOERTZEL_DEBUG
	  static int n;

	  if( !( n++ & 0xFFF ) ) {
	      std::cerr << "output " << noutput_items << " w " << fl.get_freq() << std::endl;
	      std::cerr << " 0: " << comp[ 0 ] << std::endl;
	      std::cerr << " 1: " << comp[ 1 ] << std::endl;
	      std::cerr << " 2: " << comp[ 2 ] << std::endl;
	      std::cerr << " 3: " << comp[ 3 ] << std::endl;
	      std::cerr << ( locked ? "locked" : "not locked" ) << std::endl;
	      std::cerr << "phase_err: " << phase_err << " period: " << period << std::endl;
	  }
#endif
      }

      // Tell runtime system how many output items we produced.
      return done;
    }

  } /* namespace goertzel */
} /* namespace gr */

