/* -*- c++ -*- */
/* 
 * Copyright 2017 University of Utah and the Flux Group
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_GOERTZEL_DEFSK_IMPL_H
#define INCLUDED_GOERTZEL_DEFSK_IMPL_H

#include <goertzel/defsk.h>

namespace gr {
    namespace goertzel {
	class filter {
	private:
	    double w, cos_w;
	    gr_complexd s2, s1, s0;
	    double decay;
	public:
	    filter( double w0, double decay0 );
	    void feed( gr_complexd s );
	    double get_freq();
	    void set_freq( double w_new );
	    void set_decay( double decay_new );
	    double power();
	};
	    
	class defsk_impl : public defsk {
	private:
	    double fc, rate, shift;
	    filter fl, fh, fll, flp, flm, fhp, fhm, fhh;
	    double diff;
	    int index, period, phase;
	    int locked;
	    double comp[ 4 ];
	    double phase_err;
	
	public:
	    defsk_impl( double fc, double rate, double carrier, double shift );
	    ~defsk_impl();

	    int work( int noutput_items, gr_vector_const_void_star &input_items,
		      gr_vector_void_star &output_items );
	};	
    }
}

#endif /* INCLUDED_GOERTZEL_DEFSK_IMPL_H */

