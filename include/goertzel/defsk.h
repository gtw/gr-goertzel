/* -*- c++ -*- */
/* 
 * Copyright 2017 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_GOERTZEL_DEFSK_H
#define INCLUDED_GOERTZEL_DEFSK_H

#include <goertzel/api.h>
#include <gnuradio/sync_decimator.h>

namespace gr {
  namespace goertzel {

    /*!
     * \brief <+description of block+>
     * \ingroup goertzel
     *
     */
    class GOERTZEL_API defsk : virtual public gr::sync_decimator
    {
     public:
      typedef boost::shared_ptr<defsk> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of goertzel::defsk.
       *
       * To avoid accidental use of raw pointers, goertzel::defsk's
       * constructor is in a private implementation
       * class. goertzel::defsk::make is the public interface for
       * creating new instances.
       */
      static sptr make( double fc, double rate, double carrier, double shift );
    };

  } // namespace goertzel
} // namespace gr

#endif /* INCLUDED_GOERTZEL_DEFSK_H */

